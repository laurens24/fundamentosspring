package com.fundamentosplatzi.springboot.fundamentos.bean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MyBeanWithDependenyImplement implements  MyBeanWithDependency {

    Log LOGGER= LogFactory.getLog(MyBeanWithDependenyImplement.class);

    private MyOperation myoperation;

    public MyBeanWithDependenyImplement(MyOperation myoperation) {
        this.myoperation = myoperation;
    }

    @Override
    public void printWithDependency() {
        LOGGER.info("Hemos ingresado al metodo printWithDependency");
        int numero=1;
        LOGGER.debug("El numero enviado como parametro a la dependencia operación es: "+ numero);
        System.out.println(myoperation.sum(numero));
        System.out.println("Hola desde la implementación de un bean con dependencia");
    }
}
