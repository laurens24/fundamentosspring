package com.fundamentosplatzi.springboot.fundamentos;

import com.fundamentosplatzi.springboot.fundamentos.bean.MyBean;
import com.fundamentosplatzi.springboot.fundamentos.bean.MyBeanImplement;
import com.fundamentosplatzi.springboot.fundamentos.bean.MyBeanWithDependency;
import com.fundamentosplatzi.springboot.fundamentos.bean.MyBeanWithProperties;
import com.fundamentosplatzi.springboot.fundamentos.component.ComponentDependency;
import com.fundamentosplatzi.springboot.fundamentos.component.person.PersonFullName;
import com.fundamentosplatzi.springboot.fundamentos.entity.User;
import com.fundamentosplatzi.springboot.fundamentos.pojo.UserPojo;
import com.fundamentosplatzi.springboot.fundamentos.repository.UserRepository;
import com.fundamentosplatzi.springboot.fundamentos.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class FundamentosApplication implements CommandLineRunner {

	Log LOGGER = LogFactory.getLog(FundamentosApplication.class);
	private ComponentDependency componentDependency;
	private MyBean myBean;
	private MyBeanWithDependency myBeanWithDependency;
	private PersonFullName personFullName;
	private MyBeanWithProperties myBeanWithProperties;
	private UserPojo userPojo;
	private UserRepository userRepository;
	private UserService userService;

	public FundamentosApplication(@Qualifier("componentTwoImplement") ComponentDependency componentDependency,
								  MyBean myBean,
								  MyBeanWithDependency myBeanWithDependency,
								  PersonFullName personFullName,
								  MyBeanWithProperties myBeanWithProperties,
								  UserPojo userPojo,
								  UserRepository userRepository,
								  UserService userService)  {
		this.componentDependency=componentDependency;
		this.myBean=myBean;
		this.myBeanWithDependency=myBeanWithDependency;
		this.personFullName=personFullName;
		this.myBeanWithProperties=myBeanWithProperties;
		this.userPojo=userPojo;
		this.userRepository=userRepository;
		this.userService=userService;
	}

	public static void main(String[] args) {
		SpringApplication.run(FundamentosApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//ejemplosAnteriores();
		saveUsersInDataBase();
		getInformationJpqlFromUser();
		saveWithErrorTransactional();
	}

	private void saveWithErrorTransactional() {
		User test1 = new User("TestTransactional1", "TestTransactional1@domain.com", LocalDate.now());
		User test2 = new User("TestTransactional2", "TestTransactional2@domain.com", LocalDate.now());
		User test3 = new User("TestTransactional3", null, LocalDate.now());
		//User test3 = new User("TestTransactional3", "TestTransactional4@domain.com", LocalDate.now()); //ejemplo dos
		User test4 = new User("TestTransactional4", "TestTransactional4@domain.com", LocalDate.now());
		List<User> users = Arrays.asList(test1, test2, test3, test4);

		try {
			userService.saveTransactional(users);
			users.stream().forEach(user -> LOGGER.info("Mi usuario registrado " + user.toString()));
		} catch (RuntimeException e) {
			LOGGER.error("La siguiente exepcion ocurrio durante la ejecución del metodo para registrar usuarios");
			LOGGER.error(e.getMessage());
		}
		userService.getAllUsers().stream()
				.forEach(user -> LOGGER.info("Este es el usuario dentro del metodo transacional "+user));
	}

	private void  getInformationJpqlFromUser() {
		LOGGER.info("Usuario con el metodo  getInformationJpqlFromUser " + userRepository.findByUserEmail("john@domain.com")
						.orElseThrow(()->new RuntimeException("No se encontro el usuario")));

		userRepository.findAndSort("user", Sort.by("id").descending())
				.stream()
				.forEach(user -> LOGGER.info("Usuario con metodo findAndSort "+ user));

		userRepository.findByName("John")
				.stream()
				.forEach(user -> LOGGER.info("Usuario con metodo findByName "+user));

		LOGGER.info("Usuario con metodo findByEmailAndName "+userRepository.findByEmailAndName("julie@domain.com","Julie")
				.orElseThrow(()->new RuntimeException(("Usuario no encontrado"))));

		userRepository.findByNameLike("%u%").stream()
				.forEach(user -> LOGGER.info("Usuario findByNameLike "+ user));


		userRepository.findByNameOrEmail(null,"user10@domain.com").stream()
				.forEach(user -> LOGGER.info("Usuario findByNameOrEmail "+ user));

		userRepository.findByBrthDateBetween(LocalDate.of(2021,3,1), LocalDate.of(2021,4,2))
				.stream()
				.forEach(user -> LOGGER.info("usuario con findByBrthDateBetween "+ user));

		userRepository.findByNameLikeOrderByIdDesc("%user%")
				.stream()
				.forEach(user -> LOGGER.info("Usuario encontrado con like y ordenado "+ user));

		userRepository.findByNameContainOrderByIdDesc("user")
				.stream()
				.forEach(user -> LOGGER.info("Usuario encontrado con like y ordenado "+ user));

		LOGGER.info("El usuario a partir del named parameter es "+ userRepository.getAllByBirthDateAndEmail(LocalDate.of(2021,07,20),"daniela@domain.com")
				.orElseThrow(()-> new RuntimeException("No se encontró el usuario a partir de named parameter")));


	}

	private void saveUsersInDataBase() {
		User user1= new User("John", "john@domain.com", LocalDate.of(2021,03,20));
		User user2= new User("Julie", "julie@domain.com", LocalDate.of(2021,05,21));
		User user3= new User("Daniela", "daniela@domain.com", LocalDate.of(2021,07,20));
		User user4= new User("user4", "user4@domain.com", LocalDate.of(2021,07,22));
		User user5= new User("user5", "user5@domain.com", LocalDate.of(2021,11,21));
		User user6= new User("user6", "user6@domain.com", LocalDate.of(2021,02,10));
		User user7= new User("user7", "user7@domain.com", LocalDate.of(2021,03,15));
		User user8= new User("user8", "user8@domain.com", LocalDate.of(2021,04,14));
		User user9= new User("user9", "user9@domain.com", LocalDate.of(2021,06,12));
		User user10= new User("user10", "user10@domain.com", LocalDate.of(2021,05,2));
		User user11= new User("user11", "user11@domain.com", LocalDate.of(2021,04,5));
		User user12= new User("user12", "user12@domain.com", LocalDate.of(2021,02,6));

		List<User> list = Arrays.asList(user1, user3, user3, user4, user5, user6, user7, user9, user10, user11, user12);
		list.stream().forEach(userRepository::save);

	}



	private void ejemplosAnteriores() {
		componentDependency.saludar();
		myBean.print();
		myBeanWithDependency.printWithDependency();
		personFullName.fullName("Laurens","Sanchez");
		System.out.println(myBeanWithProperties.function());
		System.out.println(userPojo.getEmail() + " " + userPojo.getPassword());

		try {

			int value= 10/0;
			LOGGER.debug("Mi valor "+ value);
		} catch (Exception e) {
			LOGGER.error("Esto es un error del dividir por cero " + e.getStackTrace());
		}
	}
}
