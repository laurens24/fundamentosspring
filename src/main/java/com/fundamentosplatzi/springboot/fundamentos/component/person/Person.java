package com.fundamentosplatzi.springboot.fundamentos.component.person;

import org.springframework.stereotype.Component;

@Component
public class Person implements PersonFullName{
    @Override
    public void fullName(String name, String lastName) {
        System.out.println(name + " " + lastName);
    }
}
