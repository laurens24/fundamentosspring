package com.fundamentosplatzi.springboot.fundamentos.component.person;

public interface PersonFullName {
    void fullName(String name, String lastName);
}
